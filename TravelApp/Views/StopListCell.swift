//
//  StopListCell.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 03.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class StopListCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var stopCellView: UIView!
    @IBOutlet weak var stopTitleLabel: UILabel!
    @IBOutlet weak var stopSubTitleLabel: UILabel!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet var ratingStarsCollection: [UIImageView]!
    @IBOutlet weak var sumOfSpendMoneyLabel: UILabel!
    @IBOutlet weak var currencySpendMoneyLabel: UILabel!
    @IBOutlet weak var transportTypeImage: UIImageView!
    
    // MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
