//
//  UIView+CoreKit.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 12.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import UIKit

//запомнить. это ООП
extension UIView {
    @IBInspectable var testCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
}
