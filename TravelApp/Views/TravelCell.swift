//
//  TravelListTableViewCell.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 26.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class TravelCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var travelSubTitle: UILabel!
    @IBOutlet weak var travelTitleLabel: UILabel!
    @IBOutlet weak var ratingStars: UIStackView!
    @IBOutlet var ratingStarsCollection: [UIImageView]!
    @IBOutlet weak var travelCellView: UIView!
}
