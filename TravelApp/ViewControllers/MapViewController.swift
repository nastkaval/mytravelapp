//
//  MapViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 23.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import MapKit

// MARK: - Protocols
protocol MapViewControllerDelegate {
    func mapControllerDidSelectPoint(_ point: MKPointAnnotation)
}

class MapViewController: UIViewController {
    
    // MARK: - Properties
    var locationArray: [MKPointAnnotation]?
    //    var createController: CreateStopViewController? - при подключении протокола, данный объект не нужен
    var delegate: MapViewControllerDelegate?
    
    // MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - Actions
    @IBAction func mapClicked (_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: mapView)
        let tapPoint = mapView.convert(point, toCoordinateFrom: view)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = tapPoint
        mapView.addAnnotation(annotation)
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(annotation)
    }
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        if let selectedPoint = mapView.annotations.first as? MKPointAnnotation {
            delegate?.mapControllerDidSelectPoint(selectedPoint)
        }
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        let saveButton = UIBarButtonItem (barButtonSystemItem: .save, target: self, action: #selector (saveButtonClicked(_:)))
        self.navigationItem.rightBarButtonItems = [saveButton]
        if let array = locationArray {
            for point in array {
                mapView.addAnnotation(point)
            }
        }
    }
    
    // MARK: - Functions
    
}
