//
//  UIViewController+CoreKit.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 17.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

extension UIViewController { //func for retrieve controller
    static func getFromStoryboard (withID id: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: "TravelAppMain", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: id)
        return controller
    }
}
