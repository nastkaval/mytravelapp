//
//  CreateStopViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 10.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift


protocol CreateStopViewControllerDelegate {
    func createStopControllerDidCreateStop (_ stop: Stop)
    func sumOfRatingStops (_ rating: Int)
}

class CreateStopViewController: UIViewController {
    
    // MARK: - Properties
    var stopListDelegate: CreateStopViewControllerDelegate?
    var stopDidCreateClosure: ((Stop) -> Void)?
    var stop = Stop()
    var travel = Travel()
    var geolocation = Geolocation()
    var currencyType : Currency = .dollar
    
    
    // MARK: - Outlets
    @IBOutlet weak var nameTextField: UITextField! //name of city
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var moneySpendLabel: UILabel! //the money sum
    @IBOutlet weak var currencyLabel: UILabel! //label with $,€,₱ near the money sum
    @IBOutlet weak var descTextView: UITextView! //description
    @IBOutlet weak var geolocationLabel: UILabel!
    @IBOutlet weak var transportTypeOutlet: UISegmentedControl!
    
    // MARK: - Actions: Buttons
    @IBAction func saveButtonClicked(_ sender: Any) { //button SAVE on the right of top
        if let nameText = nameTextField.text{
            stop.name = nameText
            print ("nameText \(nameText)")
            print ("stop.name \(stop.name!)")
        }
        if let deckText = descTextView.text { //description
            stop.desc = deckText
        }
        if let ratingValue = ratingLabel.text, let ratingValueInt = Int(ratingValue) {
            stop.rating = ratingValue
            stopListDelegate?.sumOfRatingStops(ratingValueInt)
        }
        if let money = moneySpendLabel.text { //the money sum
            stop.spendMoney = money
        }
        //        stopListDelegate?.createStopControllerDidCreateStop(stop) - старая реализация
        stopDidCreateClosure?(stop)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func cancelClicked(_ sender: Any) { //button CANCEL on the left of top
        navigationController?.popViewController(animated: true)
    }
    @IBAction func moneyButtonClicked(_ sender: Any) { //button "chose currency" -> SpendMoneyVC
        if let moneyController = UIViewController.getFromStoryboard(withID: "SpendMoneyViewController") as? SpendMoneyViewController {
            moneyController.delegateOfCreateStopVC = self
            navigationController?.pushViewController(moneyController, animated: true)
        }
    }
    @IBAction func addGeolocationClicked(_ sender: UIButton) { //open MapVC
        let minsk = MKPointAnnotation()
        minsk.coordinate = CLLocationCoordinate2D(latitude: 53.925, longitude: 27.508)
        let moscow = MKPointAnnotation()
        moscow.coordinate = CLLocationCoordinate2D(latitude: 55.668, longitude: 37.689)
        if let mapController = UIViewController.getFromStoryboard(withID: "MapViewController") as? MapViewController {
            mapController.locationArray = [minsk, moscow]
            mapController.delegate = self
            navigationController?.pushViewController(mapController, animated: true)
        }
    }
    
    // MARK: - Actions: Elements
    @IBAction func ratingClicked(_ sender: UIStepper) {
        var number = 0
        number = Int(sender.value)
        ratingLabel.text = String(number)
    }
    @IBAction func transportTypeSegmentedControllerClicked(_ sender: UISegmentedControl) { //пока что отключен
        //        switch sender.selectedSegmentIndex {
        //        case 0:
        //            stop.transportTypeImage = UIImage (named: "icon_plain")
        //            stop.transportTypeSenderValue = 0
        //        case 1:
        //            stop.transportTypeImage = UIImage (named: "icon_train")
        //            stop.transportTypeSenderValue = 1
        //        case 2:
        //            stop.transportTypeImage = UIImage (named: "icon_car")
        //            stop.transportTypeSenderValue = 2
        //        default:
        //            break
        //        }
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //        currencyTypeMoney()
        sendSaveInformationForEdit ()
    }
    
    // MARK: - Functions
    func sendSaveInformationForEdit () { //еще в процессе перевода на реалм
        let realm = try! Realm()
        if stop.name?.isEmpty == false {
            try! realm.write {
                nameTextField.text = stop.name
            }
        }
        if stop.currency?.isEmpty == false {
            currencyLabel.text = stop.currency
        }
        if stop.desc?.isEmpty == false {
            descTextView.text = stop.desc
        }
        if stop.rating?.isEmpty == false {
            ratingLabel.text = stop.rating
        }
        if stop.spendMoney?.isEmpty == false {
            moneySpendLabel.text = stop.spendMoney
        }
        //            if stop.transportTypeSenderValue != nil {
        //                switch stop.transportTypeSenderValue {
        //                case 0:
        //                    transportTypeOutlet.selectedSegmentIndex = 0
        //                case 1:
        //                    transportTypeOutlet.selectedSegmentIndex = 1
        //                case 2:
        //                    transportTypeOutlet.selectedSegmentIndex = 2
        //                default:
        //                    break
        //                }
        //            }
        //        }
        
        
    }
    func userSpentMoney (_ moneyCount: String) {
        moneySpendLabel.text = moneyCount
    }
    func currencyTypeMoney () { //func for label with $,€,₱ near the money sum
        switch currencyType {
        case .dollar:
            currencyLabel.text = "$"
        case .euro:
            currencyLabel.text = "€"
        case .ruble:
            currencyLabel.text = "₱"
        case .none:
            break
        }
    }
}

// MARK: - Extensions
extension CreateStopViewController: MapViewControllerDelegate {
    func mapControllerDidSelectPoint(_ point: MKPointAnnotation) {
        geolocationLabel.text = "ш.\(point.coordinate.latitude) д.\(point.coordinate.longitude)"
    }
}
