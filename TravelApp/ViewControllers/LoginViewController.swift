//
//  LoginViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 17.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    // MARK: - Properties
    var emailData: String?
    
    // MARK: - Outlets
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    // MARK: - Actions
    @IBAction func forgotPasswordClicked(_ sender: Any) {
        if let forgotController = UIViewController.getFromStoryboard(withID: "ForgotPasswordViewController"){
            navigationController?.pushViewController(forgotController, animated: true)
        }
    }
    @IBAction func loginClicked(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            ApiManager.instance.signIn(email: email, password: password) { (userEmail, error) in
                if error == nil {
                    print("ok: \(userEmail)")
                } else {
                    //пускаем в приложение
                }
            }
        }
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        email()
        navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Functions
    func email () {
        if emailData != nil {
            emailTextField.text = emailData
        }
    }
}
