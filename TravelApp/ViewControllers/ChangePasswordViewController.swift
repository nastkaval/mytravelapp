//
//  ChangePasswordViewController.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 23.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import Firebase



class ChangePasswordViewController: UIViewController {
    
    //MARK:- Properties
    var emailData: String?
    
    //MARK:- Outlets
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    //MARK:- Actions
    @IBAction func saveNewPasswordClicked(_ sender: Any) {
        if let password = newPasswordTextField.text, let confirmPassword = confirmPasswordTextField.text {
            if password != confirmPassword {
                newPasswordTextField.backgroundColor = .red
            } else {
                if let controller = UIViewController.getFromStoryboard(withID: "LoginViewController") as? LoginViewController {
                    controller.emailData = self.emailData
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    
    //MARK:- LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

    //MARK:- Extensions
extension ChangePasswordViewController: ForgotPasswordViewControllerDelegate {
    func sendingEmail(_ email: String) {
        emailData = email
    }
}
