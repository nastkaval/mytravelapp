//
//  EnterVerificationNumberViewController.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 23.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class EnterVerificationNumberViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var verificationCodeTextField: UITextField!
    
    //MARK: - Actions
    @IBAction func changePasswordClicked(_ sender: Any) {
        let text = "Полученный и распаршенный от сервера код"
        if text == verificationCodeTextField.text {
            if let controller = UIViewController.getFromStoryboard(withID: "ChangePasswordViewController") as? ChangePasswordViewController{
                navigationController?.pushViewController(controller, animated: true)
            }
        } else {
            
        }
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
