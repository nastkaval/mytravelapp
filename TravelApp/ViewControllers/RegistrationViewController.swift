//
//  RegistrationViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 19.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class RegistrationViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var passLabel: UITextField!
    @IBOutlet weak var confirmPassLabel: UITextField!
    
    // MARK: - Actions
    @IBAction func newRegistrationButtonClicked(_ sender: Any) {
        if let password = passLabel.text, let confirmPassword = confirmPassLabel.text, let email = emailLabel.text {
            if password != confirmPassword {
                passLabel.backgroundColor = .red
                confirmPassLabel.backgroundColor = .red
            } else {
                ApiManager.instance.signUp(email: email, password: password) { (userEmail, error) in
                    if error == nil {
                        //пускаем в программу
                    } else {
                        //поставить алерт контроллер
                    }
                }
            }
        }
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
    }
}
