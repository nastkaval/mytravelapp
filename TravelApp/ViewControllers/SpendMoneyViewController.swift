//
//  MoneyViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 19.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class SpendMoneyViewController: UIViewController {
    
    // MARK: - Properties
    var delegateOfCreateStopVC: CreateStopViewController?
    var selectedCurrency: Currency = .dollar
    var stop = Stop()
    
    // MARK: - Outlets
    @IBOutlet weak var spendMoneyTextField: UITextField!
    @IBOutlet weak var currencyLabel: UILabel!
    
    // MARK: - Actions
    @IBAction func saveMoneyButtonClicked(_ sender: Any) {
        if let text = spendMoneyTextField.text {
            delegateOfCreateStopVC?.userSpentMoney(text)
        }
        delegateOfCreateStopVC?.currencyType = selectedCurrency
        navigationController?.popViewController(animated: true)
        delegateOfCreateStopVC?.currencyTypeMoney()
    }
    @IBAction func currencySegmentedController(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            selectedCurrency = .dollar
            currencyLabel.text = "$"
            stop.currency = "$"
        case 1:
            selectedCurrency = .euro
            currencyLabel.text = "€"
            stop.currency = "€"
        case 2:
            selectedCurrency = .ruble
            currencyLabel.text = "₱"
            stop.currency = "₱"
        default:
            selectedCurrency = .dollar
            break
        }
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
    }
}
