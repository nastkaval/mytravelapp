//
//  WelcomeViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 17.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import Firebase

class WelcomeViewController: UIViewController {
    
    
    // MARK: - Outlets
    @IBOutlet weak var moreWaysToLoginButton: UIButton!
    
    // MARK: - Actions
    @IBAction func registerClicked(_ sender: Any) {
        if let registerController = UIViewController.getFromStoryboard(withID: "RegistrationViewController") as? RegistrationViewController {
            navigationController?.pushViewController(registerController, animated: true)
        }
    }
    @IBAction func loginClicked(_ sender: Any) {
        if let loginController = UIViewController.getFromStoryboard(withID: "LoginViewController") as? LoginViewController {
            navigationController?.pushViewController(loginController, animated: true)
        }
    }
    @IBAction func moreWaysClicked(_ sender: Any) {
        if let stopController = UIViewController.getFromStoryboard(withID: "CreateStopViewController") as? CreateStopViewController {
            navigationController?.pushViewController(stopController, animated: true)
        }
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
        remoteConfig.fetch { (status, error) in
            remoteConfig.activateFetched()
            if let buttontitle = remoteConfig["button_title"].stringValue {
                self.moreWaysToLoginButton.setTitle(buttontitle, for: .normal)
            }
            let isButtonHidden = remoteConfig["button_hidden"].boolValue
            self.moreWaysToLoginButton.isHidden = isButtonHidden
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
}
