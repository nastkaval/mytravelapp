//
//  StopsListViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 03.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit

class StopsListViewController: UIViewController {
    
    // MARK: - Properties
    var travel: Travel!
    var stopsArray: [Stop] = []
    
    // MARK: - Outlets
    @IBOutlet weak var stopListTableView: UITableView!
    @IBOutlet weak var backgroundTextLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - Actions
    @IBAction func addButton(_ sender: Any) {
        if let controller = UIViewController.getFromStoryboard(withID: "CreateStopViewController") as? CreateStopViewController {
            //            controller.stopListDelegate = self
            controller.stopDidCreateClosure = { stop in
                //                self.travel.stops.append(stop)
                DataBaseManager.instance.addStop(stop, to: self.travel) //добавляем в бд 
                DataBaseManager.instance.saveToDatabase(object: [self.travel]) //обновляем инфу в БД
                self.stopsArray = DataBaseManager.instance.getStopsFromDatabase(object: self.travel)
                self.stopListTableView.reloadData()
            }
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopListTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        hiddenLabel()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        stopListTableView.reloadData()
        print (travel.rating)
    }
    
    // MARK: - Functions
    func hiddenLabel() {
        if travel.stops.count != 0 {
            backgroundTextLabel.isHidden = true
        }else {
            backgroundTextLabel.isHidden = false
        }
    }
}

// MARK: - Extension
extension StopsListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stopsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopListCell", for: indexPath) as! StopListCell
        cell.stopCellView.layer.shadowColor = UIColor.gray.cgColor
        cell.stopCellView.layer.shadowOpacity = 2
        cell.stopCellView.layer.shadowOffset = .zero
        cell.stopCellView.layer.shadowRadius = 5
        //        cell.stopCellView.layer.shadowPath = UIBezierPath(rect: yourView.bounds).cgPath
        let stop = stopsArray[indexPath.row]
        cell.stopTitleLabel.text = stop.name
        cell.stopSubTitleLabel.text = stop.desc
        cell.currencySpendMoneyLabel.text = stop.currency
        cell.sumOfSpendMoneyLabel.text = stop.spendMoney
        //        cell.transportTypeImage.image = stop.transportTypeImage
        cell.selectionStyle = .none
        if let ratingString = stop.rating, let rating = Int(ratingString) {
            if let stars = cell.ratingStarsCollection {
                if rating == 1 {
                    stars[0].isHighlighted = true
                }
                if rating == 2 {
                    stars[0].isHighlighted = true
                    stars[1].isHighlighted = true
                }
                if rating == 3 {
                    stars[0].isHighlighted = true
                    stars[1].isHighlighted = true
                    stars[2].isHighlighted = true
                }
                if rating == 4 {
                    stars[0].isHighlighted = true
                    stars[1].isHighlighted = true
                    stars[2].isHighlighted = true
                    stars[3].isHighlighted = true
                }
                if rating == 5 {
                    stars[0].isHighlighted = true
                    stars[1].isHighlighted = true
                    stars[2].isHighlighted = true
                    stars[3].isHighlighted = true
                    stars[4].isHighlighted = true
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = UIViewController.getFromStoryboard(withID: "CreateStopViewController") as? CreateStopViewController{
            controller.stop = stopsArray[indexPath.row]
            controller.descTextView?.text = stopsArray[indexPath.row].name
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            let stop = stopsArray[indexPath.row]
            DataBaseManager.instance.removeStop(stop)
            stopListTableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            stopListTableView.reloadData()
        }
    }
}


extension StopsListViewController: CreateStopViewControllerDelegate {
    func sumOfRatingStops (_ rating: Int) {
        travel.rating = travel.rating + rating
        print (travel.rating)
    }
    func createStopControllerDidCreateStop(_ stop: Stop) {
        stopsArray.append(stop)
        stopListTableView.reloadData()
        print ("travel.stops \(travel.stops)")
        print ("stop \(stop)")
        hiddenLabel()
        navigationController?.isNavigationBarHidden = false
    }
}

extension StopsListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            stopsArray = DataBaseManager.instance.getStopsFromDatabase(object: travel)
            stopListTableView.reloadData()
            return
        }
        stopsArray = stopsArray.filter({ stop -> Bool in
            stop.name!.lowercased().contains(searchText.lowercased())
            
        })
        stopListTableView.reloadData()
    }
}
