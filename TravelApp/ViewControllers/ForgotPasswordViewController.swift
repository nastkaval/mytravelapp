//
//  ForgotPasswordViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 19.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import Firebase

protocol ForgotPasswordViewControllerDelegate {
    func sendingEmail (_ email: String)
}

class ForgotPasswordViewController: UIViewController {
    
    // MARK:- Properties
    var delegate: ForgotPasswordViewControllerDelegate?
    
    // MARK:- Outlets
    @IBOutlet weak var emailTextField: UITextField!
    
    // MARK:- Actions
    @IBAction func resetClicked (_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail: "\(emailTextField.text)") { (error) in
            if error == nil {
                if let text = self.emailTextField.text {
                    self.delegate?.sendingEmail(text)
                }
                print ("новый пароль отправлен успешно")
            } else {
                print("К сожалению, новый пароль не отправлен")
            }
        }
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:- Functions
}


