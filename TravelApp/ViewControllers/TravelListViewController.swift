//
//  TravelListViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 26.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift

class TravelListViewController: UIViewController {
    
    // MARK: - Properties
    var travels : [Travel] = []
    
    // MARK: - Outlets
    @IBOutlet weak var travelSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundTextOutlet: UILabel!
    
    // MARK: - Actions
    @IBAction func addTravelClicked(_ sender: Any) {
        let alertController = UIAlertController(title: "Введите страну и описание путешествия", message: nil, preferredStyle: .alert)
        let addNewTravel = UIAlertAction(title: "Сохранить", style: .default) { (_) in
            let nameTextField = alertController.textFields![0] as UITextField
            let deskTextField = alertController.textFields![1] as UITextField
            let travel = Travel()
            if let name = nameTextField.text {
                travel.name = name
                print (name)
            }
            if let desc = deskTextField.text {
                travel.desc = desc
            }
            DataBaseManager.instance.saveToDatabase(object: [travel.self])
            self.travels.append(travel)
            self.tableView.reloadData()
            self.hiddenLabel()
        }
        addNewTravel.isEnabled = false
        alertController.addAction(addNewTravel)
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel) { (_) in
        }
        alertController.addAction(cancelAction)
        alertController.addTextField { (textField) in
            textField.placeholder = "Название страны"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Описание поездки"
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                addNewTravel.isEnabled = textField.text != ""
            }
        }
        present(alertController, animated: true)
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() { //важное правило при работе с таблицами: если в таблице не отображаются
        // ячейки то первым делом нужно проверить установлен ли делегат и дата сорс.
        super.viewDidLoad() // вью показан, но без верстки
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        travels = DataBaseManager.instance.getAllFromDatabase(Travel.self)
        self.tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) //вью почти показалась
        self.tableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated) //вью показалась и уже есть верстка
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated) //срабатывает за долю секунды до закрытия
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated) //срабатывает сразу после того как экран исчез
    }
    deinit { //срабатывает, когда экран освобождается из памяти = полностью удален
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews() // когда меняются значения констрейнтов
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator) //при изменении ориентации экрана
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning() //срабатывает, когда происходит перелимит по использованию ОП, утечка памяти 
    }
    // MARK: - Functions
    func hiddenLabel() {
        if travels.count != 0 {
            backgroundTextOutlet.isHidden = true
        } else {
            backgroundTextOutlet.isHidden = false
        }
    }
}

// MARK: - Extensions
extension TravelListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travels.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TravelCell
        let travel = travels[indexPath.row]
        cell.travelCellView.layer.shadowColor = UIColor.gray.cgColor
        cell.travelCellView.layer.shadowOpacity = 2
        cell.travelCellView.layer.shadowOffset = .zero
        cell.travelCellView.layer.shadowRadius = 5
        cell.selectionStyle = .none
        cell.travelSubTitle.text = travel.desc
        cell.travelTitleLabel.text = travel.name
        if travel.rating != 0 {
            let middleRating = round(Double(travel.rating / travel.stops.count))
            if let stars = cell.ratingStarsCollection {
                if middleRating == 1 {
                    stars[0].isHighlighted = true
                    cell.ratingStars.isHidden = false
                }
                if middleRating == 2 {
                    stars[0].isHighlighted = true
                    stars[1].isHighlighted = true
                    cell.ratingStars.isHidden = false
                }
                if middleRating == 3 {
                    stars[0].isHighlighted = true
                    stars[1].isHighlighted = true
                    stars[2].isHighlighted = true
                    cell.ratingStars.isHidden = false
                }
                if middleRating == 4 {
                    stars[0].isHighlighted = true
                    stars[1].isHighlighted = true
                    stars[2].isHighlighted = true
                    stars[3].isHighlighted = true
                    cell.ratingStars.isHidden = false
                }
                if middleRating >= 5 {
                    stars[0].isHighlighted = true
                    stars[1].isHighlighted = true
                    stars[2].isHighlighted = true
                    stars[3].isHighlighted = true
                    stars[4].isHighlighted = true
                    cell.ratingStars.isHidden = false
                }
            }
        }
        else {
            cell.ratingStars.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let controller = UIViewController.getFromStoryboard(withID: "StopsListViewController") as? StopsListViewController{
            navigationController?.pushViewController(controller, animated: true)
            controller.travel = travels[indexPath.row]
            controller.stopsArray =  DataBaseManager.instance.getStopsFromDatabase(object: travels[indexPath.row])
        }
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            let travel = travels[indexPath.row]
            DataBaseManager.instance.removeTravel(travel)
            travels.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            tableView.reloadData()
        }
    }
}

extension TravelListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            travels = DataBaseManager.instance.getAllFromDatabase(Travel.self)
            tableView.reloadData()
            return
        }
        travels = DataBaseManager.instance.getAllFromDatabase(Travel.self).filter({ travel -> Bool in
            travel.name.lowercased().contains(searchText.lowercased())
            
        })
        tableView.reloadData()
    }
}


