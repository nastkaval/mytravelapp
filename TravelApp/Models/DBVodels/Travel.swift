//
//  RealmTravel.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 31.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import RealmSwift
import Foundation

class Travel: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var desc: String = ""
    @objc dynamic var rating: Int = 0
    let stops = List<Stop>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
