//
//  RealmGeo.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 29.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class Geo: Object {
    @objc dynamic var longitude: String?
    @objc dynamic var latitude: String?
}
