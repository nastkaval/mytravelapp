//
//  StopViewController.swift
//  Lesson_8_TravelApp
//
//  Created by Nastassia  Kavalchuk on 19.04.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.


import UIKit
import RealmSwift

//class Stop {
//    var name: String?
//    var rating: String?
//    var spendMoney: String?
//    var transportTypeImage: UIImage?
//    var transportTypeSenderValue: Int?
//    var desc: String?
//    var currency: String?
//}
//class Geolocation {
//    var longitude: Double = 0.0
//    var latitude: Double = 0.0
//}
enum Currency {
    case dollar
    case euro
    case ruble
    case none
}

