//
//  RealmGeolocation.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 01.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import RealmSwift

class Geolocation: Object {
    
    @objc dynamic var longitude: Double = 0.0
    @objc dynamic var latitude: Double = 0.0
}
