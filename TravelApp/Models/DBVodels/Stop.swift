//
//  RealmStop.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 31.05.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import UIKit
import RealmSwift
import Foundation

class Stop: Object {
    @objc dynamic var id: String = UUID().uuidString //генерация рандомного ID 
    @objc dynamic var name: String?
    @objc dynamic var rating: String?
    @objc dynamic var spendMoney: String?
//    @objc dynamic var transportTypeImage: Data?
    @objc dynamic var transportTypeSenderValue: Int = 0
    @objc dynamic var desc: String?
    @objc dynamic var currency: String?
    
    override static func primaryKey() -> String? {
        return "id"
}
}






