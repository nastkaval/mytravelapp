//
//  ApiManager.swift
//  TravelApp
//
//  Created by Nastassia  Kavalchuk on 26.06.2019.
//  Copyright © 2019 Nastassia  Kavalchuk. All rights reserved.
//

import Foundation
import Firebase

class ApiManager {
    static let instance = ApiManager()
    
    func signIn(email: String, password: String, onComplete: ((String?, Error?) -> Void)?) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if error == nil {
                let user = result?.user
                print(user?.email)
                onComplete?(user?.email, nil)
            } else {
                onComplete?(nil, error)
                print ("Ошибка логина")
            }
        }
    }
    func signUp(email: String, password: String, onComplete: ((String?, Error?) -> Void)?) {
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if error == nil {
                let user = result?.user
                print (user?.email)
                onComplete?(user?.email, nil)
            }else {
                onComplete?(nil, error)
                print("Не смогли зарегистрировать Вас")
            }
        }
    }
}
